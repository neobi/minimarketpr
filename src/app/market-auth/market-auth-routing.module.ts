import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MarketAuthPage } from './market-auth.page';

const routes: Routes = [
  {
    path: '',
    component: MarketAuthPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MarketAuthPageRoutingModule {}
