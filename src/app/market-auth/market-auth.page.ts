import {Component, OnInit} from '@angular/core';
import {NativeStorage} from '@ionic-native/native-storage';
import {StorageService} from '../services/storage.service';
import {AuthConstants} from '../config/auth-constants';
import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})

@Component({
    selector: 'app-market-auth',
    templateUrl: './market-auth.page.html',
    styleUrls: ['./market-auth.page.scss'],
})

export class MarketAuthPage implements OnInit {
    marketId = null;

    constructor(private storageService: StorageService) {
    }

    ngOnInit() {

    }

    ionViewWillEnter() {
        this.setMarketIdOnPageEnter();
    }

    async setMarketIdOnPageEnter() {
        await this.storageService.getItem(AuthConstants.AUTH).then(data => {
            if (data) {
                this.marketId = data;
                this.storageService.companyId = data;
            }
        });
    }

    setMarketId() {
        this.storageService.setItem(AuthConstants.AUTH, this.marketId);
        this.storageService.companyId = this.marketId;
    }

    ionViewWillLeave() {
        this.setMarketId();
    }

}
