import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MarketAuthPageRoutingModule } from './market-auth-routing.module';

import { MarketAuthPage } from './market-auth.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MarketAuthPageRoutingModule
  ],
  declarations: [MarketAuthPage]
})
export class MarketAuthPageModule {}
