import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MarketAuthPage } from './market-auth.page';

describe('MarketAuthPage', () => {
  let component: MarketAuthPage;
  let fixture: ComponentFixture<MarketAuthPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarketAuthPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MarketAuthPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
