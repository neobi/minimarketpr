import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
    selector: 'app-report',
    templateUrl: './report.page.html',
    styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit {

    isLoaded = false;
    customMonthNames = 'Январь, Февраль, Март, Апрель, Май, Июнь, Июль, Август, Сентябрь, Октябрь, Ноябрь, Декабрь';
    todayDate = new Date().toDateString();

    constructor(private router: Router) {
    }

    ionViewWillEnter() {
        this.todayDate = new Date().toDateString();
    }

    ngOnInit() {
    }

    detailPage(id) {
        this.router.navigate(['/report'], id);
    }

}
