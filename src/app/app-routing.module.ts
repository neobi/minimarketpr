import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';
import {HomeGuard} from './guards/home.guard';

const routes: Routes = [
    {path: '', redirectTo: 'auth', pathMatch: 'full'},
    {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomePageModule),
    },
    {
        path: 'total-sales',
        loadChildren: () => import('./total-sales/total-sales.module').then(m => m.TotalSalesPageModule)
    },
    {
        path: 'confirmation-popover',
        loadChildren: () => import('./confirmation-popover/confirmation-popover.module').then(m => m.ConfirmationPopoverPageModule)
    },
    {
        path: 'products-to-buy',
        loadChildren: () => import('./products-to-buy/products-to-buy.module').then(m => m.ProductsToBuyPageModule)
    },
    {
        path: 'categories',
        loadChildren: () => import('./categories/categories.module').then(m => m.CategoriesPageModule)
    },
    {
        path: 'categories/:categoryId',
        loadChildren: () => import('./goods-by-category/goods-by-category.module').then(m => m.GoodsByCategoryPageModule)
    },

    {
        path: 'auth',
        loadChildren: () => import('./auth/auth.module').then(m => m.AuthPageModule)
    },
    {
        path: 'market-auth',
        loadChildren: () => import('./market-auth/market-auth.module').then(m => m.MarketAuthPageModule)
    },
  {
    path: 'report',
    loadChildren: () => import('./report/report.module').then( m => m.ReportPageModule)
  },
  {
    path: 'report/:reportId',
    loadChildren: () => import('./report-details/report-details.module').then( m => m.ReportDetailsPageModule)
  },  {
    path: 'statistics',
    loadChildren: () => import('./statistics/statistics.module').then( m => m.StatisticsPageModule)
  },


];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
