import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {PopoverController} from '@ionic/angular';

@Component({
  selector: 'app-confirmation-popover',
  templateUrl: './confirmation-popover.page.html',
  styleUrls: ['./confirmation-popover.page.scss'],
})
export class ConfirmationPopoverPage implements OnInit {

    constructor(private router: Router, private popoverCtrl: PopoverController) {
    }

    ngOnInit() {
    }

    async close() {
        this.router.navigate(['/products-to-buy']);
        await this.popoverCtrl.dismiss();
    }
}

