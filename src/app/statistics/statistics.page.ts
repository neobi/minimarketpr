import {Component, OnInit} from '@angular/core';

@Component({
    selector: 'app-statistics',
    templateUrl: './statistics.page.html',
    styleUrls: ['./statistics.page.scss'],
})
export class StatisticsPage implements OnInit {

    isLoaded = false;
    customMonthNames = 'Январь, Февраль, Март, Апрель, Май, Июнь, Июль, Август, Сентябрь, Октябрь, Ноябрь, Декабрь';
    todayDate = new Date().toDateString();
    customPickerOptions: any;

    constructor() {
        this.customPickerOptions = {
            buttons: [
                {
                    text: 'Отмена',
                    role: 'cancel'
                },
                {
                    text: 'Ок',
                    handler: (event) => console.log(event)
                }
            ]
        };
    }

    ionViewWillEnter() {
        this.todayDate = new Date().toDateString();
    }
    ngOnInit() {
    }

}
