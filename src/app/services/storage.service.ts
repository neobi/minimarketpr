import {Injectable} from '@angular/core';
import {NativeStorage} from '@ionic-native/native-storage';

@Injectable({
    providedIn: 'root'
})
export class StorageService {

    userData: any = {};
    companyId = '';

    constructor() {
    }

    setItem(key: string, value: any) {
        NativeStorage.setItem(key, value).then(() => console.log('stored'));
    }

    getItem(key: string) {
        return NativeStorage.getItem(key);
    }

    removeItem(key: string) {
        NativeStorage.remove(key)
            .then(() => console.log('Logged out'));
    }

    clearItem() {
        NativeStorage.clear()
            .then(data => console.log('Cleared successfully'));
    }
}
