import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {StorageService} from './storage.service';
import {AuthConstants} from '../config/auth-constants';
import {HttpService} from './http.service';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService {


    constructor(private httpService: HttpService, private storageService: StorageService) {
    }

    // checkAuth(companyId, login: string, password: string) {
    //     return this.httpService.checkAuth(companyId, login, password);
    // }

    login(companyId, userData): Observable<any> {
        return this.httpService.checkAuth(companyId, userData);
    }

    logOut() {
        this.storageService.removeItem(AuthConstants.USER_DATA);
    }
}
