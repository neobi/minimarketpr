import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class HttpService {

    constructor(private http: HttpClient) {
    }

    checkAuth(companyId, userData): Observable<any> {
        const url = `https://cloud.ehost.tj/index.php/api/check_personal?company_id=${companyId}&login=${userData.userName}&password=${userData.password}`;
        return this.http.get<any>(url);
    }

    getCategories(companyId, userData): Observable<any> {
        const url = `https://cloud.ehost.tj/index.php/api/get_category?company_id=${companyId}&login=${userData.userName}&password=${userData.password}`;
        return this.http.get<any>(url);
    }

    getAvailableProducts(companyId, userData, page = 1): Observable<any> {
        
        const url = `https://cloud.ehost.tj/index.php/api/get_product_list?company_id=${companyId}&login=${userData.userName}&password=${userData.password}&page=${page}`;
        return this.http.get<any>(url);
    }

    getAllProductsByCategory(companyId, userData, categoryId, page = 1): Observable<any> {
        const url = `https://cloud.ehost.tj/index.php/api/get_product_all?category_id=${categoryId}&company_id=${companyId}&login=${userData.userName}&password=${userData.password}&page=${page}`;
        return this.http.get<any>(url);
    }

    buyProducts(companyId, userData, data): Observable<any> {
        // const header = new HttpHeaders()
        // header.set('Content-Type': 'application/json',)
        // const httpOptions = {
        //     headers: new HttpHeaders({
        //         'Content-Type': 'application/json',
        //         'Access-Control-Allow-Origin' : '*',
        //         'Access-Control-Allow-Methods' : 'GET, POST, PUT, DELETE'
        //     })
        //   };
        const url = `https://cloud.ehost.tj/index.php/api/cart?company_id=${companyId}&login=${userData.userName}&password=${userData.password}`;
        return this.http.post<any>(url, JSON.stringify(data));
    }

    search(companyId, userData, categoryId, productId): Observable<any> {
        const url = `https://cloud.ehost.tj/index.php/api/get_search?q=${productId}&category_id=${categoryId}&company_id=${companyId}&login=${userData.userName}&password=${userData.password}`;
        return this.http.get(url);
    }

}
