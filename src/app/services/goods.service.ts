import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {AuthConstants} from '../config/auth-constants';
import {StorageService} from './storage.service';
import {ToastController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class GoodsService {
    products = [];
    totPrice = 0;
    goodsInBasket = [];

    constructor(private httpService: HttpService,
                private storageService: StorageService) {
    }

    getAvailableProducts(page) {
        this.products = [];
        this.httpService.getAvailableProducts(this.storageService.companyId, this.storageService.userData, page)
            .subscribe(data => {
                if (data['code'] != -1 && data['product'].length > 0) {
                    this.products.push(data['product']);
                    return true;
                } else if (data['product'].length === 0 && data['code'] != -1) {
                    return false;
                }
            });
    }
}
