import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductsToBuyPageRoutingModule } from './products-to-buy-routing.module';

import { ProductsToBuyPage } from './products-to-buy.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductsToBuyPageRoutingModule
  ],
  declarations: [ProductsToBuyPage]
})
export class ProductsToBuyPageModule {}
