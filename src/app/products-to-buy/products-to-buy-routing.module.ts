import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductsToBuyPage } from './products-to-buy.page';

const routes: Routes = [
  {
    path: '',
    component: ProductsToBuyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductsToBuyPageRoutingModule {}
