import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductsToBuyPage } from './products-to-buy.page';

describe('ProductsToBuyPage', () => {
  let component: ProductsToBuyPage;
  let fixture: ComponentFixture<ProductsToBuyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductsToBuyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductsToBuyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
