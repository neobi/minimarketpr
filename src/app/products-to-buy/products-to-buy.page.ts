import {Component, OnInit, Injectable} from '@angular/core';
import {GoodsService} from '../services/goods.service';
import {AlertController, LoadingController} from '@ionic/angular';
import {HttpService} from '../services/http.service';
import {AuthConstants} from '../config/auth-constants';
import {ActivatedRoute, Router} from '@angular/router';
import {StorageService} from '../services/storage.service';

@Component({
    selector: 'app-products-to-buy',
    templateUrl: './products-to-buy.page.html',
    styleUrls: ['./products-to-buy.page.scss'],
})
@Injectable({
    providedIn: 'root'
})
export class ProductsToBuyPage implements OnInit {
    allPr = [];
    totPrice = 0;

    constructor(private goodsService: GoodsService,
                private httpService: HttpService,
                private storageService: StorageService,
                private route: ActivatedRoute,
                public loadingController: LoadingController,
                private alertCtrl: AlertController,
                private router: Router) {
    }

    ionViewWillEnter() {
        this.allPr = this.goodsService.goodsInBasket;
        this.totPrice = this.goodsService.totPrice;
    }

    ngOnInit() {

    }

    async cashConfirmButton() {
        const loading = await this.loadingController.create({
            spinner: 'crescent',
            message: 'Please wait...',
            translucent: true,
            cssClass: 'custom-class custom-loading'
        });

        const alert = await this.alertCtrl.create({
            mode: 'ios',
            header: 'Вы уверены закончить текущий шоппинг?',
            buttons: [
                {
                    text: 'Отменить',
                    role: 'cancel',
                    cssClass: 'secondary'
                },
                {
                    text: 'Да',
                    handler: () => {
                        loading.present();

                        let products = [];
                        this.goodsService.goodsInBasket.forEach(el => {
                            products.push({product_id: el.product_id, count: el.count});
                        });
                        this.httpService.buyProducts(this.storageService.companyId, this.storageService.userData, products)
                            .subscribe(res => {
                                    if (res['code'] == 1) {
                                        this.router.navigate(['home']);
                                        this.goodsService.totPrice = 0;
                                        this.goodsService.goodsInBasket = [];
                                        loading.dismiss();
                                    } else {
                                        loading.dismiss();
                                        this.alert('Произошла ошибка!');
                                    }
                                },
                                error => console.log(error)
                            );
                    }
                }
            ]
        });
        await alert.present();
    }

    async alert(msg: string) {
        const alert = await this.alertCtrl.create({
            mode: 'ios',
            header: msg,
            buttons: [
                {
                    text: 'Отменить',
                    role: 'cancel',
                    cssClass: 'secondary'
                }
            ]
        });
        await alert.present();
    }
}
