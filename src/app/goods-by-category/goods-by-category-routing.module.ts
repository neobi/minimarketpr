import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GoodsByCategoryPage } from './goods-by-category.page';

const routes: Routes = [
  {
    path: '',
    component: GoodsByCategoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GoodsByCategoryPageRoutingModule {}
