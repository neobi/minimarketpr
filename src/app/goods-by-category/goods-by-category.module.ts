import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GoodsByCategoryPageRoutingModule } from './goods-by-category-routing.module';

import { GoodsByCategoryPage } from './goods-by-category.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GoodsByCategoryPageRoutingModule
  ],
  declarations: [GoodsByCategoryPage]
})
export class GoodsByCategoryPageModule {}
