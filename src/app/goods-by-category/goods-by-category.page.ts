import {Component, OnInit, SimpleChanges, OnChanges} from '@angular/core';
import {GoodsService} from '../services/goods.service';
import {AlertController, LoadingController, ToastController} from '@ionic/angular';
import {TotalSalesPage} from '../total-sales/total-sales.page';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpService} from '../services/http.service';
import {StorageService} from '../services/storage.service';
import {AuthConstants} from '../config/auth-constants';
import {BarcodeScanner} from '@ionic-native/barcode-scanner/ngx';
import {subscribeOn} from 'rxjs/operators';


@Component({
    selector: 'app-goods-by-category',
    templateUrl: './goods-by-category.page.html',
    styleUrls: ['./goods-by-category.page.scss'],
})
export class GoodsByCategoryPage implements OnInit, OnChanges {
    barcID: any;
    totalProducts = [];
    categoryId;
    isLoaded = false;
    currentPage = 1;

    constructor(private goodsService: GoodsService,
                public alertController: AlertController,
                private tSales: TotalSalesPage,
                private router: Router,
                public toastController: ToastController,
                private route: ActivatedRoute,
                private storageService: StorageService,
                public loadingController: LoadingController,
                private httpService: HttpService,
                private barcodeScanner: BarcodeScanner) {
    }


    ionViewWillEnter() {
        this.getAllPr();
    }

    ionViewWillLeave() {
        this.totalProducts = [];
    }

    ngOnChanges(changes: SimpleChanges) {
    }

    ngOnInit() {

    }


    async getAllPr(page = 1) {
        this.isLoaded = false;
        this.categoryId = this.route.snapshot.params.categoryId;
        this.httpService.getAllProductsByCategory(this.storageService.companyId, this.storageService.userData, this.categoryId, page)
            .subscribe(res => {
                    if (res['code'] == 1) {
                        this.totalProducts = this.totalProducts.concat(res['product']);
                        this.goodsService.products = this.totalProducts;
                        this.isLoaded = true;
                    }
                }
            );
    }

    scan() {
        this.barcodeScanner.scan().then(barcodeData => {
            this.barcID = barcodeData.text;
            this.httpService.search(this.storageService.companyId, this.storageService.userData, this.categoryId, barcodeData.text)
                .subscribe(res => {
                        if (res['code'] == 1 && res['product'].length !== 0) {
                            this.totalProducts = res['product'];
                        } else {
                            this.totalProducts = [];
                        }
                    }
                );

        }).catch(err => {
            console.log('Error', err);
        });
    }

    findProduct() {
        // if (this.totalProducts.find(x => x.barcodeId == this.barcID) != null) {
        //     this.totalProducts = [];
        //     this.totalProducts.push(this.goodsService.getProductsByCategory(this.categ).find(x => x.barcodeId == this.barcID));
        // }
    }

    checkForEmpty(event) {
        if (event == '') {
            this.totalProducts = this.goodsService.products;
        }
    }

    loadData(event) {
        setTimeout(() => {
            event.target.complete();
            this.currentPage++;

            this.httpService.getAllProductsByCategory(this.storageService.companyId, this.storageService.userData, this.categoryId, this.currentPage)
                .subscribe(res => {
                        if (res['code'] == 1 && res['product'].length !== 0) {
                            this.totalProducts = this.totalProducts.concat(res['product']);
                            this.goodsService.products = this.totalProducts;
                        } else {
                            event.target.disabled = true;
                            this.alert('Больше нет товаров!');
                        }
                    }
                );
        }, 500);
    }

    async confirmAddProductToBuyList(el) {
        if (el.total > 0) {
            el['count'] = 1;
            const alert = await this.alertController.create({
                mode: 'ios',
                header: 'Вы уверены что хотите добавить этот продукт в корзину?',
                // message: 'Message <strong>text</strong>!!!',
                buttons: [
                    {
                        text: 'Отменить',
                        role: 'cancel',
                        cssClass: 'secondary'
                    },
                    {
                        text: 'Да',
                        handler: () => {
                            if (this.goodsService.goodsInBasket.length !== 0) {
                                if ((this.goodsService.goodsInBasket.filter(x => x.product_id == el.product_id)).length == 0) {
                                    this.goodsService.goodsInBasket.push(el);
                                    this.goodsService.totPrice = Number(this.goodsService.totPrice) + Number(el.product_retail_price);
                                    this.presentToast();
                                } else {
                                    this.alert('Вы уже добавили этот продукт');
                                }
                            } else {
                                this.goodsService.goodsInBasket.push(el);
                                this.goodsService.totPrice = Number(this.goodsService.totPrice) + Number(el.product_retail_price);
                                this.presentToast();
                            }
                        }

                    }
                ]
            });
            await alert.present();
        } else {
            this.alert('Нет товара');
        }
    }

    async presentToast() {
        const toast = await this.toastController.create({
            mode: 'ios',
            message: 'Успешно добавлено.',
            duration: 2500,
            color: 'dark',
            buttons: [
                {
                    text: 'Корзина',
                    handler: () => {
                        this.router.navigate(['/total-sales']);
                    },
                    side: 'end',
                    icon: 'checkmark'
                }
            ]
        });
        toast.present();
    }

    async alert(msg: string) {
        const alert = await this.alertController.create({
            mode: 'ios',
            header: msg,
            // message: 'Message <strong>text</strong>!!!',
            buttons: [
                {
                    text: 'Ок',
                    role: 'cancel',
                    cssClass: 'secondary'
                }
            ]
        });
        await alert.present();
    }

    searchProducts() {
        this.barcID = this.barcID.trim();
        this.httpService.search(this.storageService.companyId, this.storageService.userData, this.categoryId, this.barcID)
            .subscribe(res => {
                    if (res['code'] == 1 && res['product'].length !== 0) {
                        this.totalProducts = res['product'];
                    } else {
                        this.totalProducts = [];
                    }
                }
            );
    }
}
