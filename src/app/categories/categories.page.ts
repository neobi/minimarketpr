import {Component, OnInit} from '@angular/core';
import {GoodsService} from '../services/goods.service';
import {Router} from '@angular/router';
import {StorageService} from '../services/storage.service';
import {Platform, ToastController} from '@ionic/angular';
import {HttpService} from '../services/http.service';

@Component({
    selector: 'app-categories',
    templateUrl: './categories.page.html',
    styleUrls: ['./categories.page.scss'],
})
export class CategoriesPage {

    allCateg = [];
    isLoaded = false;

    constructor(private goodsService: GoodsService,
                private router: Router,
                private httpService: HttpService,
                private storageService: StorageService,
                private  toastCtrl: ToastController) {
    }


    ionViewWillEnter() {
        this.getCategories();
    }

    getCategories() {
        this.httpService.getCategories(this.storageService.companyId, this.storageService.userData)
            .subscribe(data => {
                if (data['code'] == 1) {
                    this.allCateg = data['category'];
                    this.isLoaded = true;
                    console.log('isloaded true' + this.isLoaded);
                }
            }, error => {
                this.toastCT('Произошла ошибка!');
            });
        console.log('isloaded false ' + this.isLoaded);
    }

    async toastCT(txt: string) {
        const toast = await this.toastCtrl.create({
            message: txt,
            duration: 2500,
            color: 'dark',
        });
        await toast.present();
    }

    getAvailablePr() {
        this.storageService.setItem('allPr', false);
        this.goodsService.getAvailableProducts(1);
        this.router.navigate(['goods-by-category']);
    }

}
