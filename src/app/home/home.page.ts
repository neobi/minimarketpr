import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {TotalSalesPage} from '../total-sales/total-sales.page';
import {AuthenticationService} from '../services/authentication.service';
import {StorageService} from '../services/storage.service';
import {AuthConstants} from '../config/auth-constants';
import {AlertController, Platform} from '@ionic/angular';

@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

    userName: string;
    backBtn: any;

    constructor(private router: Router,
                private authService: AuthenticationService,
                private storageService: StorageService,
                private platform: Platform,
                public alertController: AlertController) {
    }

    ionViewWillEnter() {
        this.backBtn = this.platform.backButton.subscribe(() => {
            this.appExitAlert();
        });
    }

    ngOnInit() {
        this.setName();
        console.log(this.storageService.userData);
    }

    setName() {
        this.userName = (this.storageService.userData).userName;
    }

    logOff() {
        this.router.navigate(['']);
        this.authService.logOut();
    }

    async appExitAlert() {
        const alert = await this.alertController.create({
            mode: 'ios',
            header: 'Вы уверены что хотите выйти?',
            buttons: [
                {
                    text: 'Отмена',
                    role: 'cancel',
                    cssClass: 'secondary'
                },
                {
                    text: 'Ок',
                    cssClass: 'secondary',
                    handler: () => {
                        navigator['app'].exitApp();
                    }
                }
            ]
        });
        await alert.present();
    }

    ionViewDidLeave() {
        this.backBtn.unsubscribe();
    }
}
