import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Observable} from 'rxjs';
import {StorageService} from '../services/storage.service';
import {AuthConstants} from '../config/auth-constants';

@Injectable({
    providedIn: 'root'
})
export class HomeGuard implements CanActivate {

    constructor(private storageService: StorageService, private router: Router) {
    }

    canActivate(): Promise<boolean> {
        return new Promise(resolve => {
            this.storageService.getItem(AuthConstants.USER_DATA)
                .then(data => {
                    if (data) {
                        resolve(true);
                    } else {
                        this.router.navigate(['']);
                        resolve(false);
                    }
                })
                .catch(error => {
                    resolve(false);
                });
        });
    }
}
