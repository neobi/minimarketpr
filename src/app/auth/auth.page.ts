import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NativeStorage} from '@ionic-native/native-storage';
import {AlertController, LoadingController} from '@ionic/angular';
import {AuthenticationService} from '../services/authentication.service';
import {AuthConstants} from '../config/auth-constants';
import {StorageService} from '../services/storage.service';

@Component({
    selector: 'app-auth',
    templateUrl: './auth.page.html',
    styleUrls: ['./auth.page.scss'],
})
export class AuthPage {

    userData = {
        userName: '',
        password: ''
    };

    constructor(private router: Router,
                private alertController: AlertController,
                private authService: AuthenticationService,
                private storageService: StorageService,
                public loadingController: LoadingController) {
    }

    ionViewWillEnter() {
        this.setCompanyIdInStorage();
    }

    async setCompanyIdInStorage() {
        await this.storageService.getItem(AuthConstants.AUTH)
            .then(data => {
                if (data) {
                    this.storageService.companyId = data;
                }
            });
    }

    validateInputs() {
        let username = this.userData.userName;
        let password = this.userData.password;

        return (this.userData.userName && this.userData.password && username.length > 0
            && password.length >= 4 && password.length < 100
            && (this.storageService.companyId).length !== 0);
    }

    async loadingFunc() {
        const loading = await this.loadingController.create({
            spinner: 'crescent',
            message: 'Signing in...',
            translucent: true,
            cssClass: 'custom-class custom-loading'
        });
        loading.present();
    }

    login() {
        if (this.validateInputs()) {
            this.loadingFunc().then(() => {
                this.authService.login(this.storageService.companyId, this.userData)
                    .subscribe(data2 => {
                        if (Number(data2.code) === 1) {
                            this.storageService.setItem(AuthConstants.USER_DATA, this.userData);
                            this.loadingController.dismiss();
                            this.storageService.userData = this.userData;

                            console.log(this.storageService.userData);
                            this.router.navigate(['home'], {replaceUrl: true});
                            this.userData = {
                                userName: '',
                                password: ''
                            };
                        } else {
                            this.loadingController.dismiss();
                            this.alertMessage();
                        }
                    });

            });

        } else {
            this.alertMessage();
        }
    }

    async alertMessage() {
        const alert2 = await this.alertController.create({
            mode: 'ios',
            header: 'Пожалуйста проверьте снова!',
            message: '<strong>Не забудьте указать Id магазина</strong>!',
            buttons: [
                {
                    text: 'Ок',
                    role: 'cancel',
                    cssClass: 'secondary'
                }
            ]
        });
        await alert2.present();
    }

    ionViewWillLeave() {
        this.storageService.getItem(AuthConstants.USER_DATA)
            .then(data => {

                console.log(this.storageService.userData);
            });
    }
}
