import {Component, Injectable, OnInit} from '@angular/core';
import {GoodsService} from '../services/goods.service';
import {AlertController, PopoverController} from '@ionic/angular';
import {ConfirmationPopoverPage} from '../confirmation-popover/confirmation-popover.page';
import {Router} from '@angular/router';
import {AuthConstants} from '../config/auth-constants';
import {StorageService} from '../services/storage.service';
import {HttpService} from '../services/http.service';
import {BarcodeScanner} from '@ionic-native/barcode-scanner/ngx';


@Component({
    selector: 'app-total-sales',
    templateUrl: './total-sales.page.html',
    styleUrls: ['./total-sales.page.scss'],
})
@Injectable({
    providedIn: 'root'
})
export class TotalSalesPage implements OnInit {

    barcId = '';
    allProducts = [];
    foundProducts = [];
    isFound = false;
    totalPrice: any = 0;

    constructor(private goodsService: GoodsService,
                private popoverCtrl: PopoverController,
                private router: Router,
                private alertController: AlertController,
                private storageService: StorageService,
                private httpService: HttpService,
                private barcodeScanner: BarcodeScanner) {
    }

    ngOnInit() {
    }

    ionViewWillEnter() {
        this.allProducts = this.goodsService.goodsInBasket;
        console.log(this.goodsService.totPrice);
        this.totalPrice = this.goodsService.totPrice;
    }


    changePrice(id) {
        let count1;
        count1 = (document.getElementById('searchInp' + id) as HTMLInputElement).value;
        count1 = Number(count1);
        console.log(count1 + ' count1');
        this.allProducts.forEach((el, index) => {
            if (el.product_id == id) {
                if (el.total < count1) {
                    (document.getElementById('searchInp' + id) as HTMLInputElement).value = el.total;
                    el.total = Number(el.total);
                    el.count = el.total;
                    this.allProducts[index] = el;
                } else if (count1 < 1 || count1 == '') {
                    (document.getElementById('searchInp' + id) as HTMLInputElement).value = '1';
                    el.count = 1;
                    this.allProducts[index] = el;
                } else {
                    el.count = count1;
                    this.allProducts[index] = el;
                }
            }
        });
        this.saveData();
    }

    async popoverConf() {
        this.goodsService.goodsInBasket = this.allProducts;
        this.goodsService.totPrice = this.totalPrice;
        const popover = await this.popoverCtrl.create({
            component: ConfirmationPopoverPage
        });
        return await popover.present();
    }

    scan() {
        this.barcodeScanner.scan().then(barcodeData => {
            this.barcId = barcodeData.text;

            this.httpService.search(this.storageService.companyId, this.storageService.userData, 0, barcodeData.text)
                .subscribe(res => {
                        if (res['code'] == 1 && res['product'].length !== 0) {
                            this.foundProducts = res['product'];
                            this.isFound = true;
                        } else {
                            this.foundProducts = [];
                            this.isFound = false;
                        }
                    }
                );
        }).catch(err => {
            console.log('Error', err);
        });
    }

    searchProduct() {
        this.barcId = this.barcId.trim();
        if (this.barcId === '') {
            this.foundProducts = [];
            (document.getElementById('sr') as HTMLInputElement).style.display = 'none';
        } else {
            this.isFound = true;
            this.httpService.search(this.storageService.companyId, this.storageService.userData, 0, this.barcId)
                .subscribe(res => {
                        if (res['code'] == 1 && res['product'].length !== 0) {
                            this.foundProducts = res['product'];
                            this.isFound = true;
                        } else {
                            this.foundProducts = [];
                            this.isFound = false;
                        }
                        (document.getElementById('sr') as HTMLInputElement).style.display = 'block';
                    },
                    error => console.log(error)
                );
        }
    }


    decreaseCount(id) {
        this.allProducts.forEach((el, index) => {
            if (el.product_articul == id) {
                if (el.count <= 1) {
                    (document.getElementById('searchInp' + id) as HTMLInputElement).value = '1';
                } else {
                    el.count--;
                    this.allProducts[index] = el;
                }
            }
        });
        this.saveData();
    }

    increaseCount(id) {
        this.allProducts.forEach((el, index) => {
            if (el.product_articul == id) {
                if (el.total <= el.count) {
                    (document.getElementById('searchInp' + id) as HTMLInputElement).value = el.total;
                } else {
                    el.count++;
                    this.allProducts[index] = el;
                }
            }
        });
        this.saveData();
    }

    async addToList(product: any) {
        product['count'] = 1;
        this.foundProducts = [];
        (document.getElementById('sr') as HTMLInputElement).style.display = 'none';
        if (this.allProducts.find(prod => prod.product_id == product.product_id) == null) {
            if (product.total > 0) {
                this.allProducts.push(product);
                this.saveData();
            } else {
                const alert = await this.alertController.create({
                    header: 'Нет товара',
                    buttons: [
                        {
                            text: 'Ок',
                            role: 'cancel',
                            cssClass: 'secondary'
                        }
                    ]
                });
                await alert.present();
            }
        } else {
            const alert2 = await this.alertController.create({
                header: 'Вы уже добавили этот продукт',
                buttons: [
                    {
                        text: 'Ок',
                        role: 'cancel',
                        cssClass: 'secondary'
                    }
                ]
            });
            await alert2.present();
        }

        this.barcId = '';
        (document.getElementById('sr') as HTMLInputElement).style.display = 'none';
    }

    async removeFromList(index) {
        const alert = await this.alertController.create({
            header: 'Вы уверены что хотите удалить это продукт?',
            buttons: [
                {
                    text: 'Отменить',
                    role: 'cancel',
                    cssClass: 'secondary'
                },
                {
                    text: 'Да',
                    handler: () => {
                        if (index > -1) {
                            // this.allProducts[index].currNum = 1;
                            this.allProducts.splice(index, 1);
                        }
                        if (this.allProducts.length == 0) {
                            this.totalPrice = 0;
                        } else {
                            this.saveData();
                        }

                    }

                }
            ]
        });
        await alert.present();
    }

    saveData() {
        this.goodsService.goodsInBasket = this.allProducts;
        let sum = 0;
        this.allProducts.forEach((el) => {
            sum += (el.count * el.product_retail_price);
        });
        this.totalPrice = sum;
        this.goodsService.totPrice = sum;
    }
}
