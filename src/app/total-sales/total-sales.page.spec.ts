import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TotalSalesPage } from './total-sales.page';

describe('TotalSalesPage', () => {
  let component: TotalSalesPage;
  let fixture: ComponentFixture<TotalSalesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalSalesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TotalSalesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
